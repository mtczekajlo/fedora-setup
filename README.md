# Linux Setup

## SSH key

```bash
sudo pacman -Sy git xclip
git config --global user.email "mtczekajlo@gmail.com"
git config --global user.name "Marcin Czekajło"
ssh-keygen -t rsa -b 2048 -C "mtczekajlo@gmail.com"
cat ~/.ssh/id_rsa.pub | xclip
```

## Installers

```bash
./install-packages
./install-themes
./install-scripts
```

## Cosmetics

### Gruvbox themes

- <https://github.com/TheGreatMcPain/gruvbox-material-gtk>
- <https://github.com/SylEleuth/gruvbox-plus-icon-pack.git>
- <https://github.com/sainnhe/capitaine-cursors>

```bash
./install-themes
```

### Gogh

<https://github.com/Gogh-Co/Gogh>

```bash
bash -c "$(wget -qO- https://git.io/vQgMr)"
```

![Gogh Gruvboxes](docs/gogh-gruvboxes.png)

## Rust

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

```bash
rustup component add rust-analysis
```

## Heroic Games Launcher, Steam, Lutris

<https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher>

<https://flathub.org/apps/details/com.heroicgameslauncher.hgl>

<https://copr.fedorainfracloud.org/coprs/atim/heroic-games-launcher>
